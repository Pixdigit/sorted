package sorted

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
	"gitlab.com/Pixdigit/uniqueID"
)

type testElem struct {
	id   uniqueID.ID
	Name string
}

type identifiable interface {
	ID() uniqueID.ID
}

var lastElem identifiable

func NewSampleElem(IDSpace *uniqueID.IDSpace, name string) *testElem {
	newID := IDSpace.NewID()
	elem := testElem{
		newID,
		name,
	}
	lastElem = elem
	return &elem
}
func (t testElem) ID() uniqueID.ID {
	return t.id
}

func TestNearest(t *testing.T) {

	testIDSpace := &uniqueID.IDSpace{}
	set := Set{}

	err := set.Insert(NewSampleElem(testIDSpace, "a"), 1)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(NewSampleElem(testIDSpace, "b"), 2)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(NewSampleElem(testIDSpace, "c"), 3)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(NewSampleElem(testIDSpace, "d"), 5)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(NewSampleElem(testIDSpace, "e"), 4)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	value, err := set.Nearest(3.2)
	if err != nil {
		tools.WrapErr(err, "could not get element from set", t)
	}
	tools.Test(value.(*testElem).Name == "c", "did not get corrent element", t)
	value, err = set.Nearest(3.8)
	if err != nil {
		tools.WrapErr(err, "could not get element from set", t)
	}
	tools.Test(value.(*testElem).Name == "e", "did not get corrent element", t)
	value, err = set.Nearest(8)
	if err != nil {
		tools.WrapErr(err, "could not get element from set", t)
	}
	tools.Test(value.(*testElem).Name == "d", "did not get corrent element", t)
	value, err = set.Nearest(-2)
	if err != nil {
		tools.WrapErr(err, "could not get element from set", t)
	}
	tools.Test(value.(*testElem).Name == "a", "did not get corrent element", t)
	value, err = set.Nearest(2)
	if err != nil {
		tools.WrapErr(err, "could not get element from set", t)
	}
	tools.Test(value.(*testElem).Name == "b", "did not get corrent element", t)

	tools.Test(set.Contains(lastElem), "set did not contain added element", t)
	tools.Test(set.Remove(lastElem), "could not remove element in set", t)
	tools.Test(set.Len() == 4, "set length not correct", t)

	t.Log(set.String())
}

func TestSetValue(t *testing.T) {
	testIDSpace := &uniqueID.IDSpace{}
	set := Set{}

	a := NewSampleElem(testIDSpace, "a")
	b := NewSampleElem(testIDSpace, "b")
	c := NewSampleElem(testIDSpace, "c")
	d := NewSampleElem(testIDSpace, "d")
	e := NewSampleElem(testIDSpace, "e")

	err := set.Insert(a, 1)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(b, 2)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(c, 3)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(d, 5)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}
	err = set.Insert(e, 4)
	if err != nil {
		tools.WrapErr(err, "could not insert test element", t)
	}

	set.SetValue(a, 3.5)
	set.SetValue(e, -2)

	tools.Test(set.Elems()[0] == e, "did not correctly change position when set new value", t)
	tools.Test(set.Elems()[1] == b, "did not correctly change position when set new value", t)
	tools.Test(set.Elems()[2] == c, "did not correctly change position when set new value", t)
	tools.Test(set.Elems()[3] == a, "did not correctly change position when set new value", t)
	tools.Test(set.Elems()[4] == d, "did not correctly change position when set new value", t)

}
