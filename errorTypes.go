package sorted

import (
	"fmt"
	"gitlab.com/Pixdigit/uniqueID"
)

//List is empty
type ErrListEmpty struct {
}

func (err ErrListEmpty) Error() string {
	return "list is empty"
}

type ErrDuplicateElem struct {
	DuplicateID uniqueID.ID
}

func (err ErrDuplicateElem) Error() string {
	return fmt.Sprint("Duplicate element with ID = ", err.DuplicateID)
}

type ErrNoElem struct {
	MissingID uniqueID.ID
}

func (err ErrNoElem) Error() string {
	return fmt.Sprint("no element with ID =", err.MissingID)
}
