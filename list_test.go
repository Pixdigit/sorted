package sorted

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestList(t *testing.T) {
	list := List{}
	list.Insert("a", 1)
	list.Insert("b", 2)
	list.Insert("c", 3)
	list.Insert("d", 5)
	list.Insert("e", 4)
	value, err := list.Nearest(3.2)
	if err != nil {
		tools.WrapErr(err, "could not get element from list", t)
	}
	tools.Test(value == "c", "did not get corrent element", t)
	value, err = list.Nearest(3.8)
	if err != nil {
		tools.WrapErr(err, "could not get element from list", t)
	}
	tools.Test(value == "e", "did not get corrent element", t)
	value, err = list.Nearest(8)
	if err != nil {
		tools.WrapErr(err, "could not get element from list", t)
	}
	tools.Test(value == "d", "did not get corrent element", t)
	value, err = list.Nearest(-2)
	if err != nil {
		tools.WrapErr(err, "could not get element from list", t)
	}
	tools.Test(value == "a", "did not get corrent element", t)
	value, err = list.Nearest(2)
	if err != nil {
		tools.WrapErr(err, "could not get element from list", t)
	}
	tools.Test(value == "b", "did not get corrent element", t)

	t.Log(list.String())

}
